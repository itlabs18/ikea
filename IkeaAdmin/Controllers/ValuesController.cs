﻿using IkeaWebApiLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AdminIkea.Controllers
{
    public class ValuesController : ApiController
    {
        public async Task<IEnumerable<Appointment>> Get()
        {
            return await IkeaDB.Get();
        }
    }
}
