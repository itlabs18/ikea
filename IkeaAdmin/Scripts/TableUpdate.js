﻿function appointment(id, timeInterval, usersCount) {
    this.id = id;
    this.timeInterval = timeInterval;
    this.usersCount = usersCount;
}

function viewModel() {
    var self = this;
    self.appointments = ko.observableArray([]);

    self.set = function (data) {
        var items = [];

        data.forEach(function (item) {
            items.push(new appointment(item.ID, item.TimeInterval, item.UsersCount));
        });

        self.appointments(items);
    };
}

document.addEventListener("DOMContentLoaded", function () {
    var vm = new viewModel();

    function getItems() {
        var url = location.origin + "/api/values/get";
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.onreadystatechange = function (d) {
            if (xhr.readyState != XMLHttpRequest.DONE) return;
            var result = xhr.responseText;
            if (result == null) return;
            vm.set(JSON.parse(result));
        }
        xhr.send(null);
    }

    getItems();
    ko.applyBindings(vm);

    var timer = setInterval(getItems, 5000);
});
