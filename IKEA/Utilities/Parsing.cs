﻿using IkeaWebApiLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace IKEA.Utilites
{
    public class Parsing : MainWindow
    {
       
        public static async Task<IEnumerable<string>> GetTime()
        {
            try
            {
                App.CurrentApp.IsLoading = true;
                CookieContainer cc = new CookieContainer();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://mega.ru/service/1962/omsk/ ");
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36 OPR/54.0.2952.71";
                request.Headers.Add("Upgrade-Insecure-Requests", "1");
                request.CookieContainer = cc;
                HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string result = sr.ReadToEnd();



                List<string> DateHolliDey = new List<string>();
                List<string> DateWorkDey = new List<string>();

                bool flag = false;
                Regex.Matches(result, "\\d+\\.\\s\\d+:\\d+[^0-9]+\\d+:\\d+[^.;]+").Cast<Match>().Select(d => d.Value).Where(d => !d.Contains("Школа")).ToList().ForEach(d =>
                {
                    var a = Regex.Match(d, "\\d+\\.\\s\\d+:\\d+[^0-9]+\\d+:\\d+").Value;
                    if (a.StartsWith("1."))
                        flag = !flag;
                    if (flag)
                        DateHolliDey.Add(a);
                    else
                    {
                        DateWorkDey.Add(a);
                    }
                });
                App.CurrentApp.IsLoading = false;
                if (Holiday.CheckDate())
                    return DateHolliDey.TimeFiltr();
                else
                {
                    return DateWorkDey.TimeFiltr();
                }
            }
            catch 
            {
                App.CurrentApp.HasError = true;
                return null;
            }
          finally
            {
                App.CurrentApp.IsLoading = false;
            }
        }
    }

    public static class Ecstectoin 
    {
        public static IEnumerable<string> TimeFiltr(this IEnumerable<string> list)
        {
            App.CurrentApp.IsLoading = true;
            List<string> dates = new List<string>();
            list.ToList().ForEach(d =>
             {
                 string target = string.Empty;
                 var item = new Regex("\\d+\\.\\s").Replace(d, target);
                 var startTime = DateTime.Parse(item.Split('–')[1]);
                 //var teset = startTime.AddMinutes(-15);
                 if (DateTime.Now < startTime && DateTime.Now < startTime.AddMinutes(-15))
                     dates.Add(item);
             });
            App.CurrentApp.IsLoading = false;
            return dates;
        }   
    }

}
