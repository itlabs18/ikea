﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace IKEA
{
    public static class Printer
    {
        static Printer()
        {

        }

        public static void Print(Image template, params TextData[] parameters)
        {
            PrintDialog printDialog = new PrintDialog();
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawImage(template.Source, new Rect(0, 0, template.Width, template.Height));

                foreach (var par in parameters)
                {
                    drawingContext.DrawText(
                        new FormattedText(par.Text, CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                        new Typeface(par.Font.Family, par.Font.Style, par.Font.Weight, par.Font.Stretch),
                        par.Font.Size, Brushes.Black), par.Location);
                }
            }

            printDialog.PrintVisual(drawingVisual, "");
        }
    }
}
