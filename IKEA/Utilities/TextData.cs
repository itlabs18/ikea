﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace IKEA
{
    public class TextData
    {
        public Point Location { get; set; }
        public string Text { get; set; }
        public FontParameters Font { get; set; }

        public TextData(double x, double y, string text, FontParameters font)
        {
            Location = new Point(x, y);
            Text = text;
            Font = font;
        }

        public TextData(Point location, string text, FontParameters font) :
            this(location.X, location.Y, text, font)
        {
            
        }
    }

    public class FontParameters
    {
        public FontFamily Family { get; set; }
        public FontStyle Style { get; set; }
        public FontWeight Weight { get; set; }
        public FontStretch Stretch { get; set; }
        public double Size { get; set; }
        public Color Color { get; set; }

        public FontParameters(FontFamily family, FontStyle style, FontWeight weight, FontStretch stretch, double size, Color color)
        {
            Family = family;
            Style = style;
            Weight = weight;
            Stretch = stretch;
            Size = size;
            Color = color;
        }

        public FontParameters(FontFamily family, double size) :
            this(family, FontStyles.Normal, FontWeights.Normal, FontStretches.Normal, size, Colors.Black)
        {
            
        }
    }
}
