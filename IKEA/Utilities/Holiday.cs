﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IKEA.Utilites
{
     static public class Holiday
    {
        static public bool CheckDate()
        {
            CookieContainer CC = new CookieContainer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://calendar.yoip.ru/work/" + DateTime.Now.Year + "-proizvodstvennyj-calendar.html");
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            request.Headers.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            request.CookieContainer = CC;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            string sr = reader.ReadToEnd();
            MatchCollection holiday1 = Regex.Matches(sr, "(?<=,).*(?=2018&nbsp;года. Это праздничный день.)");
            var test = holiday1.Cast<Match>().Select(f => DateTime.Parse(f.Value.Replace("&nbsp;", " "))).ToList();
            if (test.Contains(DateTime.Now.Date) ||
                (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || 
                (DateTime.Now.DayOfWeek == DayOfWeek.Sunday) || 
                (DateTime.Now.DayOfWeek == DayOfWeek.Monday) || 
                (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday) || 
                (DateTime.Now.DayOfWeek == DayOfWeek.Friday))
                return true;
            else
                return false;
        }
    }
}
