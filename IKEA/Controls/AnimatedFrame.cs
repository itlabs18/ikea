﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace IKEA.Controls
{
    public class AnimatedFrame : Frame
    {
        public AnimatedFrame()
        {
            Navigating += OnNavigating;

            Duration halfDuration = new Duration(TimeSpan.FromMilliseconds(150));
            double decAccRation = 0d;
            IEasingFunction ease = new CubicEase {EasingMode = EasingMode.EaseOut};

            #region out anim
            
            DoubleAnimation outOpacityAnimation = new DoubleAnimation(0d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(outOpacityAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(outOpacityAnimation, new PropertyPath(OpacityProperty));
            DoubleAnimation outScaleXAnimation = new DoubleAnimation(1.2d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(outScaleXAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(outScaleXAnimation, new PropertyPath("RenderTransform.(ScaleTransform.ScaleX)"));
            DoubleAnimation outScaleYAnimation = new DoubleAnimation(1.2d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(outScaleYAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(outScaleYAnimation, new PropertyPath("RenderTransform.(ScaleTransform.ScaleY)"));

            _outStoryboard = new Storyboard();
            _outStoryboard.Children.Add(outOpacityAnimation);
            _outStoryboard.Children.Add(outScaleXAnimation);
            _outStoryboard.Children.Add(outScaleYAnimation);

            #endregion

            #region in anim
            
            DoubleAnimation inOpacityAnimation = new DoubleAnimation(1d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(inOpacityAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(inOpacityAnimation, new PropertyPath(OpacityProperty));
            DoubleAnimation inScaleXAnimation = new DoubleAnimation(.8d ,1d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(inScaleXAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(inScaleXAnimation, new PropertyPath("RenderTransform.(ScaleTransform.ScaleX)"));
            DoubleAnimation inScaleYAnimation = new DoubleAnimation(.8d, 1d, halfDuration)
            {
                DecelerationRatio = decAccRation,
                EasingFunction = ease
            };
            Storyboard.SetTarget(inScaleYAnimation, _contentPresenter);
            Storyboard.SetTargetProperty(inScaleYAnimation, new PropertyPath("RenderTransform.(ScaleTransform.ScaleY)"));

            _inStoryboard = new Storyboard();
            _inStoryboard.Children.Add(inOpacityAnimation);
            _inStoryboard.Children.Add(inScaleXAnimation);
            _inStoryboard.Children.Add(inScaleYAnimation);

            #endregion
        }

        public static readonly DependencyProperty OutAnimationsProperty = DependencyProperty.Register(
            nameof(OutAnimations), typeof(TimelineCollection), typeof(AnimatedFrame),
            new PropertyMetadata(new TimelineCollection(), OnAnimationsChanged));
        
        public TimelineCollection OutAnimations
        {
            get {return (TimelineCollection)GetValue(OutAnimationsProperty); }            
            set { SetValue(OutAnimationsProperty, value); }
           
        }

        public static readonly DependencyProperty InAnimationsProperty = DependencyProperty.Register(
            nameof(InAnimations), typeof(TimelineCollection), typeof(AnimatedFrame),
            new PropertyMetadata(new TimelineCollection(), OnAnimationsChanged));

        public TimelineCollection InAnimations
        {
            get { return (TimelineCollection)GetValue(InAnimationsProperty); }
            set { SetValue(InAnimationsProperty, value); }
        }

        private static void OnAnimationsChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            AnimatedFrame frame = o as AnimatedFrame;
            if (frame == null) return;
            TimelineCollection collection = e.NewValue as TimelineCollection;
            if (collection == null) return;
            foreach (Timeline timeline in collection)
                Storyboard.SetTarget(timeline, frame._contentPresenter);
        }

        private bool _allowDirectNavigation;
        private ContentPresenter _contentPresenter;
        private NavigatingCancelEventArgs _navArgs;
        private readonly Storyboard _outStoryboard;
        private readonly Storyboard _inStoryboard;

        public override void OnApplyTemplate()
        {
            // get a reference to the frame's content presenter
            // this is the element we will fade in and out
            _contentPresenter = GetTemplateChild("PART_FrameCP") as ContentPresenter;
            if (_contentPresenter != null)
            {
                _contentPresenter.RenderTransformOrigin = new Point(.5,.5);
                _contentPresenter.RenderTransform = new ScaleTransform();
            }
            base.OnApplyTemplate();
        }

        private void OnNavigating(object sender, NavigatingCancelEventArgs e)
        {
            if (Content != null && !_allowDirectNavigation && _contentPresenter != null)
            {
                e.Cancel = true;
                _navArgs = e;
                _contentPresenter.IsHitTestVisible = false;

                _outStoryboard.Completed += FadeOutCompleted;
                _contentPresenter.BeginStoryboard(_outStoryboard);
            }
            _allowDirectNavigation = false;
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            Clock animationClock = sender as Clock;
            if (animationClock != null)
                animationClock.Completed -= FadeOutCompleted;
            
            if (_contentPresenter == null) return;

            _contentPresenter.IsHitTestVisible = true;

            _allowDirectNavigation = true;
            switch (_navArgs.NavigationMode)
            {
                case NavigationMode.New:
                    if (_navArgs.Uri == null)
                        NavigationService.Navigate(_navArgs.Content);
                    else
                        NavigationService.Navigate(_navArgs.Uri);
                    break;

                case NavigationMode.Back:
                    if(NavigationService.CanGoBack)
                        NavigationService.GoBack();
                    break;

                case NavigationMode.Forward:
                    if(NavigationService.CanGoForward)
                        NavigationService.GoForward();
                    break;

                case NavigationMode.Refresh:
                    NavigationService.Refresh();
                    break;
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new ThreadStart(() => _contentPresenter.BeginStoryboard(_inStoryboard)));
        }
    }
}
