﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using IKEA.Annotations;
using IkeaWebApiLibrary;

namespace IKEA
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application, INotifyPropertyChanged
    {        
        public static App CurrentApp => App.Current as App;
        public static App CurrentApp1 => App.Current as App;

        System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();

        public App()
        {
            timer.Interval = TimeSpan.FromMinutes(3);
            timer.Tick += Timer_Tick;
            timer.Start();

        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            var allAppointmenrt = await IkeaDB.Get();
            allAppointmenrt.ForEach(async d =>
            {
                var del = d.TimeInterval.Split('-')[1].Replace(".", ":");
                if (DateTime.Now.TimeOfDay > DateTime.Parse(del).TimeOfDay)
                    await IkeaDB.Delete(d.ID);
            });
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                OnPropertyChanged();
            }
        }

        private bool _hasError;

        public bool HasError
        {
            get { return _hasError; }
            set
            {
                _hasError = value;
                OnPropertyChanged();
            }
        }

        private bool _butt;

        public bool Button5
        {
            get { return _butt; }
            set
            {
                _butt = value;
                OnPropertyChanged();
            }
        }

        private string session;

        public string Session
        {
            get { return session; }
            set
            {
                session = value;
                OnPropertyChanged();
            }
        }

        private string time;
        public string Time
        {
            get { return time; }
            set
            {
                time = value;
                OnPropertyChanged();
            }
        }



        private ObservableCollection<string> allTimes = new ObservableCollection<string>();
        public ObservableCollection<string> AllTimes
        {
           
                get { return allTimes; }
                set { allTimes = value; OnPropertyChanged(); } 
            
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
