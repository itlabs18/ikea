﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IKEA.Utilites;
using IKEA.View;

namespace IKEA
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// ppppppppppppppppppppp
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();

        }

        System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
     
        private void Timer_Tick(object sender, EventArgs e)
        {
            var date = new StringBuilder(DateTime.Today.ToString("ddd dd.MM.yy"));
            date[0] = char.ToUpper(date[0]);
            Time2.Text = date.ToString();
            Time.Text = DateTime.Now.ToShortTimeString();
            App.CurrentApp.AllTimes = new ObservableCollection<string>(App.CurrentApp.AllTimes?.TimeFiltr());
            for (int i = 0; i < App.CurrentApp.AllTimes.Count; i++)
            {
                App.CurrentApp.AllTimes[i] = $"{i + 1}. {App.CurrentApp.AllTimes[i]}";
            }
        }
        private ICommand _errorCommand;

        public ICommand ErrorCommand => _errorCommand ?? (_errorCommand = new RelayCommand(a =>
        {
            
            App.CurrentApp.HasError = false;
            if(!(Frame.Content is Start))
            Frame.NavigationService.Navigate(new Start());
        }));
    }
}
