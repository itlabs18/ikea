﻿using IKEA.Utilites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace IKEA.View
{
    /// <summary>
    /// Логика взаимодействия для pleasantskiing.xaml
    /// </summary>
    public partial class Pleasantskiing : Page
    {
        private DispatcherTimer _timer;

        public Pleasantskiing()
        {
            InitializeComponent();
            MakePrint();
            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0, 0, 3);
            _timer.Tick += IdleTick;
            _timer.Start();
        }

        private void IdleTick(object sender, EventArgs e)
        {
            var idle = GetIdle();
            if (idle.Seconds >= 10)
            {
                NavigationService.Navigate(new Start());
            }
        }

        TimeSpan GetIdle()
        {
            var lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = (uint)Marshal.SizeOf(lastInputInfo);

            GetLastInputInfo(ref lastInputInfo);

            var lastInput = DateTime.Now.AddMilliseconds(
                -(Environment.TickCount - lastInputInfo.dwTime));
            return DateTime.Now - lastInput;
        }

        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [StructLayout(LayoutKind.Sequential)]
        internal struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }

        private RelayCommand back;
        public RelayCommand Back
        {
            get
            {
                return back ??
                  (back = new RelayCommand(obj =>
                  {
                      NavigationService.Navigate(new Pledge());
                  }));
            }
        }

        private RelayCommand menu;
        public RelayCommand Menu
        {
            get
            {
                return menu ??
                  (menu = new RelayCommand(obj =>
                  {
                      NavigationService.Navigate(new Start());
                  }));
            }
        }

        //private RelayCommand open;
        //public RelayCommand Open
        //{
        //    get
        //    {
        //        return open ??
        //          (open = new RelayCommand(obj =>
        //          {                      
        //          }));
        //    }
        //}        
        private void MakePrint()
        {
            BitmapImage bitmap = new BitmapImage();
            string path = "TemplateCheck\\ticket.png";  //Путь к файлу

            try
            {
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(path, UriKind.Relative);
                bitmap.EndInit();
            }
            catch 
            {
                //Не удалось загрузить изображение
            }

            double newWidth = 3.05 * 96;

            Image image = new Image();
            image.Source = bitmap;
            image.Width = newWidth;
            image.Height = newWidth * bitmap.Height / bitmap.Width;

          //  Затем нужно создать объект класса FontParameters(или несколько объектов, если нужен свой шрифт для каждой надписи):

        FontParameters fp = new FontParameters(new FontFamily("Verdana"), 12);
            fp.Weight = FontWeights.Bold;

            //И, наконец, вызывается метод Print:

            string date = DateTime.Now.ToShortTimeString() + " " + DateTime.Now.ToShortDateString();

            Printer.Print(image,
                new TextData(100, 70, App.CurrentApp.Time, fp),
                new TextData(84, 86, App.CurrentApp.Session.ToString(), fp),
                new TextData(18, 136, date, fp));            
        }
        
        
    }
}
