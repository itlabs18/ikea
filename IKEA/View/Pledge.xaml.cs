﻿using IKEA.Utilites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IKEA.View
{
    /// <summary>
    /// Логика взаимодействия для pledge.xaml
    /// </summary>
    public partial class Pledge : Page
    {
        public Pledge()
        {
            InitializeComponent();
        }
        private RelayCommand back;
        public RelayCommand Back
        {
            get
            {
                return back ??
                  (back = new RelayCommand(obj =>
                  {                      
                      NavigationService.Navigate(new Kids());
                  }));
            }
        }

        private RelayCommand menu;
        public RelayCommand Menu
        {
            get
            {
                return menu ??
                  (menu = new RelayCommand(obj =>
                  {
                      NavigationService.Navigate(new Start());
                  }));
            }
        }

        private RelayCommand next;
        public RelayCommand Next
        {
            get
            {
                return next ??
                  (next = new RelayCommand(obj =>
                  {
                      NavigationService.Navigate(new Pleasantskiing());
                  }));
            }
        }
    }
}
