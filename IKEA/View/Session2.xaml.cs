﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IKEA.Utilites;
using IkeaWebApiLibrary;
using System.Threading;
using System.Windows.Threading;
using System.Runtime.InteropServices;

namespace IKEA.View
{
    /// <summary>
    /// Логика взаимодействия для Session2.xaml
    /// </summary>
    public partial class Session2 : Page
    {

        private DispatcherTimer _timer;              

        public Session2()
        {
            InitializeComponent();
            Loaded += Session2_Loaded;            
        }        

        private async void Session2_Loaded(object sender, RoutedEventArgs e)
        {
            var allTimes = (await Parsing.GetTime())?.ToList();
            if (allTimes == null)
                return;
            for (int i = 0; i < allTimes.Count; i++)
            {
                allTimes[i] = $"{i + 1}. {allTimes[i]}";
            }
            App.CurrentApp.AllTimes = new ObservableCollection<string>(allTimes);
        }

        private ICommand _chooseTimeCommand;
        public ICommand ChooseTimeCommand => _chooseTimeCommand ?? (_chooseTimeCommand = new RelayCommand(async a =>
        {
            App.CurrentApp1.Button5 = true;
            string result = a.ToString().Split('.')[1];
            App.CurrentApp.IsLoading = true;
             var allWrites= await IkeaDB.Get();
            var write = allWrites.FirstOrDefault(f => f.TimeInterval == result);
            if (write!=null && write.UsersCount<50)
            {
                write.UsersCount++;
                await IkeaDB.Update(write);
                App.CurrentApp.Session = write.UsersCount.ToString();
                App.CurrentApp.Time = write.TimeInterval;
            }
            else
            {
                await IkeaDB.Add(new Appointment() { TimeInterval = result, UsersCount = 1 });
                App.CurrentApp.Session = 1.ToString();
                App.CurrentApp.Time = result;
            }
            App.CurrentApp.IsLoading = false;         
            NavigationService.Navigate(new Kids());

        }));        

        private RelayCommand menu;
        public RelayCommand Menu
        {
            get
            {
                return menu ??
                  (menu = new RelayCommand(obj =>
                  {
                      NavigationService.Navigate(new Start());
                  }));
            }
        }
        
        public static readonly DependencyProperty AllTimesProperty = DependencyProperty.Register(
            "AllTimes", typeof(ObservableCollection<string>), typeof(Session2), new PropertyMetadata(default(ObservableCollection<string>)));

        public ObservableCollection<string> AllTimes
        {
            get { return (ObservableCollection<string>)GetValue(AllTimesProperty); }
            set { SetValue(AllTimesProperty, value); }
        }
        
    }
}
