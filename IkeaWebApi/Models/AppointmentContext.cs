﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IkeaWebApi.Models
{
    public class AppointmentContext : DbContext
    {
        public AppointmentContext() : base("IkeaDB")
        { }

        public DbSet<Appointment> Items { get; set; }
    }
}