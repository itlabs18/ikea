﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IkeaWebApi.Models
{
    public class Appointment
    {
        [Key]
        public int ID { get; set; }
        public string TimeInterval { get; set; }
        public int UsersCount { get; set; }

    }
}