﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IkeaWebApi.Models;
using System.Data.Entity.Migrations;
using Newtonsoft.Json;

namespace IkeaWebApi.Controllers
{
    public class ValuesController : ApiController
    {
        public IEnumerable<Appointment> Get()
        {
            using (var db = new AppointmentContext())
            {
                return db.Items.ToList();
            }
        }

        public Appointment Get(int id)
        {
            using (var db = new AppointmentContext())
            {
                return db.Items.First(i => i.ID == id);
            }
        }

        [HttpPost]
        public bool AddOrUpdate([FromBody]Appointment value)
        {
            using (var db = new AppointmentContext())
            {
                try
                {
                    db.Items.AddOrUpdate(value);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        [HttpPost]
        public bool Delete(int id)
        {
            using (var db = new AppointmentContext())
            {
                try
                {
                    db.Items.Remove(db.Items.First(i => i.ID == id));
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
