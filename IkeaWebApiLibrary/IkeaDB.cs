﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IkeaWebApiLibrary
{
    public static class IkeaDB
    {
        private static HttpClient client = new HttpClient();
        private static string server = @"http://195.133.1.197:500";

        static IkeaDB()
        {
            
        }

        public static async Task<List<Appointment>> Get()
        {
            return await await client.GetAsync(server + @"/api/values/get").ContinueWith(async r =>
            {
                if (r.IsFaulted) return null;
                string result = await r.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Appointment>>(result);
            });
        }

        public static async Task<Appointment> Get(int id)
        {
            return await await client.GetAsync(server + @"/api/values/get/" + id.ToString()).ContinueWith(async r =>
            {
                if (r.IsFaulted) return null;
                string result = await r.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Appointment>(result);
            });
        }

        public static async Task<bool> Add(Appointment item)
        {
            var test = JsonConvert.SerializeObject(item);
            var httpContent = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");

            return await await client.PostAsync(server + @"/api/values/addorupdate", httpContent).ContinueWith(async r =>
            {
                if (r.IsFaulted) return false;
                string result = await r.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(result);
            });
        }

        public static async Task<bool> Update(Appointment item)
        {
            var test = JsonConvert.SerializeObject(item);
            var httpContent = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");

            return await await client.PostAsync(server + @"/api/values/addorupdate", httpContent).ContinueWith(async r =>
            {
                if (r.IsFaulted) return false;
                string result = await r.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(result);
            });
        }

        public static async Task<bool> Delete(int id)
        {
            return await await client.PostAsync(server + @"/api/values/delete/" + id.ToString(), null).ContinueWith(async r =>
            {
                if (r.IsFaulted) return false;
                string result = await r.Result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(result);
            });
        }
    }
}
